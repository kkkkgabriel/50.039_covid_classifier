# Matplotlib
import matplotlib.pyplot as plt

# Numpy
import numpy as np

# Pillow
from PIL import Image

# Torch
import torch
from torch.utils.data import Dataset, DataLoader
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torchvision import transforms, models

# everywhere else
import argparse
import pandas as pd
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import random
import pickle
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, multilabel_confusion_matrix, roc_curve

# custom code
from utils.datasets import *
from utils.config import *
from utils.network import *
from utils.netMixin import *
from utils.tester import *

# some misc modules
from os import listdir
from os.path import isfile, join

# init the torch seeds
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
torch.manual_seed(100)
torch.cuda.manual_seed(100)
np.random.seed(100)
random.seed(100)

parser = argparse.ArgumentParser(description="Train image classifier model")
parser.add_argument("model1_location", help="Location of first layer model")
parser.add_argument("model2_location", help="Location of second layer model")
parser.add_argument("--model1_threshold_value", default=0.5, type=float, help="Threshold value for data to be classified as positive class (normal) by the first model.")
parser.add_argument("--model2_threshold_value", default=0.5, type=float, help="Threshold value for data to be classified as positive class (non-covid). by the second model.")
parser.add_argument("--device", default='cuda', help="Device to run training on, default at 'cuda'")

args = parser.parse_args()
model1_location = args.model1_location
model2_location = args.model2_location
THRESH1 = args.model1_threshold_value
THRESH2 = args.model2_threshold_value
device = args.device

#-------------- retrieve model -----------------#
model1 = LungDetectorModel(1)
model1.load_model(model1_location)

model2 = LungDetectorModel(1)
model2.load_model(model2_location)

#-------------- prep the test data -----------------#
test_datasets = {'full': lung_dataset(),'train': lung_train_dataset(), 'validation': lung_val_dataset(), 'test': lung_test_dataset()}
_, _, testloader = load_data(test_datasets, hyperparams['dataloader']['params'])
# preprocessing
for dataset in test_datasets.values():
  dataset.set_tx(transforms.Compose([
                                  transforms.ToTensor(),
                                  transforms.Normalize([0.5], [0.5])
                                  ]))

predictions, truth = test_bin_class_model(model1, model2, 0, 2, testloader, model1_thresh=THRESH1, model2_thresh=THRESH2, device=device)
print(f'Accuracy on test set: {(predictions == truth).sum().item() / len(truth)}')