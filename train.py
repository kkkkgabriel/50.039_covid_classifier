# Matplotlib
import matplotlib.pyplot as plt

# Numpy
import numpy as np

# Pillow
from PIL import Image

# Torch
import torch
from torch.utils.data import Dataset, DataLoader
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torchvision import transforms, models

# everywhere else
import argparse
import pandas as pd
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import random
import pickle
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, multilabel_confusion_matrix, roc_curve

# custom code
from utils.datasets import *
from utils.config import *
from utils.network import *
from utils.netMixin import *
from utils.tester import *

# some misc modules
from os import listdir
from os.path import isfile, join

# init the torch seeds
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
torch.manual_seed(100)
torch.cuda.manual_seed(100)
np.random.seed(100)
random.seed(100)

parser = argparse.ArgumentParser(description="Train image classifier model")
parser.add_argument("model_layer_to_train", type=int, help="Training of first or second model. (options: 1, 2)")
parser.add_argument("save_location", help="Location to save model")
parser.add_argument("--learning_rate", type=int, default=0.001, help="set learning rate, default at 0.001")
parser.add_argument("--epochs", type=int, default=1, help="set epochs, default at 1")
parser.add_argument("--device", default='cuda', help="Device to run training on, default at 'cuda'")

args = parser.parse_args()
model_layer_to_train = args.model_layer_to_train
save_location = args.save_location
lr = args.learning_rate
eps = args.epochs
device = args.device

print('Training model for layer {} on {}'.format(model_layer_to_train, device))
#---------------- prep data for training of first model ------------------#
if model_layer_to_train == 1:
	positive_class = 0
	exclude_class = None
elif model_layer_to_train == 2:
	positive_class = 2
	exclude_class = 0

# init dataloaders
datasets = {'train': lung_bin_train_dataset(positive_class, exclude_class=exclude_class),
              'validation': lung_bin_val_dataset(positive_class, exclude_class=exclude_class),
              'test': lung_bin_test_dataset(positive_class, exclude_class=exclude_class)}
trainloader, validloader, testloader = load_data(datasets, hyperparams['dataloader']['params'])

# preprocessing
preprocessing_params = hyperparams['preprocessing']
kernel = preprocessing_params['gaussian_blur']['kernel']
degrees = preprocessing_params['affine']['degrees']
probability = preprocessing_params['flip']['probability']

rand_transforms = transforms.RandomChoice([
	transforms.RandomVerticalFlip(probability),
	transforms.GaussianBlur(kernel),
	transforms.RandomAffine(degrees)
])

for dataset in datasets.values():
	dataset.set_tx(transforms.Compose([
		rand_transforms,
		transforms.ToTensor(),
		transforms.Normalize([0.5], [0.5])
	]))

#--------------- training ---------------------#
# init the model
lungDetector1 = LungDetectorModel(1)

# hyperparams 
model_params = hyperparams['model']
criterion = nn.BCEWithLogitsLoss()
optimizer = optim.Adam(lungDetector1.parameters(), lr=lr)

# train the model
_, losses = lungDetector1.train_model(
    trainloader,
    validloader,
    eps,
    optimizer,
    criterion,
    save_location=save_location,
    device=device
)

#---------------- Save and plot the training results -------------------#
losses = torch.Tensor(losses).T

# plot losses
fig = plt.figure()
ax1 = fig.add_subplot()
ax1.set_ylabel('Losses')
ax1.set_xlabel('Epochs')
fig.set_figheight(5)
fig.set_figwidth(15)

# plot the loss values
for loss, name in zip(losses[1:],['Average training loss', 'Validation Loss']):
  plt.plot(losses[0], loss, '--o', linewidth=2, label=name)

plt.legend(loc="upper right")
plt.show() 