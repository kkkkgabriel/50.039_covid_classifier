import torch.nn as nn
import torch.nn.functional as F
import torch
from utils.netMixin import NetMixin

'''
Jeremy's model
'''
class LungDetectorModel(nn.Module, NetMixin):
	def __init__(self, n_outputs):
		super(LungDetectorModel, self).__init__()

		self.pool = nn.MaxPool2d(2, 2)
		self.dropout = nn.Dropout(0.3)

		self.conv1 = nn.Conv2d(1, 3, 3)
		self.conv2 = nn.Conv2d(3, 6, 3)
		self.conv3 = nn.Conv2d(6, 16, 5)

		self.fc1 = nn.Linear(16 * 16 * 16, 200)
		self.fc2 = nn.Linear(200, n_outputs)
		
	def forward(self, x):
		batch_size = x.size(0)
		# x = x.view(batch_size, -1, x.size(1), x.size(2))
		x = self.pool(F.relu(self.conv1(x)))
		x = self.pool(F.relu(self.conv2(x)))
		x = self.pool(F.relu(self.conv3(x)))
		x = x.view(batch_size, -1)
		x = self.dropout(x)
		x = F.relu(self.fc1(x))
		x = self.fc2(x)
		return x

'''
Gabriel's layer 1 model
'''
class covidDetector(nn.Module, NetMixin):
	def __init__(self, n_outputs):
		super(covidDetector, self).__init__()
		self.conv1 = nn.Sequential(
			nn.Conv2d(1, 16, 3),
			nn.ReLU(inplace=True),
			nn.MaxPool2d(kernel_size=3),
		)
		self.conv2 = nn.Sequential(
			nn.Conv2d(16, 128, 5),
			nn.ReLU(inplace=True),
			nn.MaxPool2d(kernel_size=3),
		)
		self.conv3 = nn.Sequential(
			nn.Conv2d(128, 256, 5),
			nn.ReLU(inplace=True),
			nn.MaxPool2d(kernel_size=3),
		)
		
		self.dropout = nn.Dropout(0.5)
		self.fc1 = nn.Linear(256 * 3 * 3, 128)
		self.fc2 = nn.Linear(128, 64)
		self.fc3 = nn.Linear(64, n_outputs)

	def forward(self, x):

		x = self.conv1(x)
		x = self.conv2(x)
		x = self.conv3(x)
		x = x.view(-1, 256 * 3 * 3)
		x = self.dropout(x)
		x = F.relu(self.fc1(x))
		x = F.relu(self.fc2(x))
		x = self.fc3(x)
		return x

'''
Gabriel's layer 2 model: residual layers
'''
class infectionDetector(nn.Module, NetMixin):
	def __init__(self, n_outputs):
		super(infectionDetector, self).__init__()
		self.innerResidual1 = nn.Sequential(
			nn.Conv2d(1, 16, 3, padding=1),
			nn.BatchNorm2d(16),
			nn.ReLU(inplace=True),
			nn.Conv2d(16, 1, 3, padding=1),
		)
		self.innerResidual2 = nn.Sequential(
			nn.Conv2d(1, 192, 3, padding=1),
			nn.BatchNorm2d(192),
			nn.ReLU(inplace=True),
			nn.Conv2d(192, 1, 3, padding=1),
		)
		self.innerResidual3 = nn.Sequential(
			nn.Conv2d(1, 256, 3, padding=1),
			nn.BatchNorm2d(256),
			nn.ReLU(inplace=True),
			nn.Conv2d(256, 1, 3, padding=1),
		)
		
		self.pooling = nn.Sequential(
			nn.MaxPool2d(kernel_size=3, stride=2),
			nn.MaxPool2d(kernel_size=3, stride=2)
		)

		self.dropout = nn.Dropout(0.5)
		self.fc1 = nn.Linear(36 * 36, 128)
		self.fc2 = nn.Linear(128, 64)
		self.fc3 = nn.Linear(64, n_outputs)

	def forward(self, x):
		# residual layer 1
		x1 = self.innerResidual1(x)
		x = x + x1
		x = F.relu(x)

		# residual layer 2
		x1 = self.innerResidual2(x)
		x = x + x1
		x = F.relu(x)

		# residual layer 3
		x1 = self.innerResidual3(x)
		x = x + x1
		x = F.relu(x)

		# pooling to reduce size
		x = self.pooling(x)
		
		# reshape for fc
		x = x.view(-1, 36 * 36)
		x = self.dropout(x)

		x = F.relu(self.fc1(x))
		x = F.relu(self.fc2(x))
		x = self.fc3(x)

		return x