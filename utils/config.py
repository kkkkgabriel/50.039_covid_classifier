'''
This file stores constants and global configuration variables
'''
# constants
MODELS_DIR = 'models/'
MAIN_RESULTS_DIR = 'results/'
EXP_RESULTS_DIR = 'analysis/alex_res_densenets_results/'
FINAL_MODEL1_NAME = 'lungDetector1'
FINAL_MODEL2_NAME = 'lungDetector2'

# settable vars
hyperparams = {
	'preprocessing': {
		"gaussian_blur": {
			"kernel": (3, 3)
		},
		"affine": {
			"degrees": 45,
		},
		"flip": {
			"probability": 50
		}
	},
	"dataloader": {
		"params": {
			'batch_size': 32,
			'shuffle': True
		}
	},
	"optimizer": {},
	"loss_function": {},
	"model": {
		"lr": 0.001,
		"eps": 10,
		"thresh1": 0.010019022040069103,
		"thresh2": 0.5608626008033752
	}
}
overwrite = False # !!! ONLY SET THIS TO TRUE IF RETRAINING EVERYTHING