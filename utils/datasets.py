from torch.utils.data import Dataset, DataLoader
from torchvision import datasets, transforms

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

'''
Dataset object for the entire lung dataset
'''
class lung_dataset(Dataset):
	def __init__(self):
		# custom attributes, nth special
		self.img_size = (150, 150)
		self.classes = {0: 'normal', 1: 'infected_covid', 2: 'infected_non-covid'}
		self.groups = ['train', 'test', 'val']
		self.dataset = {
			'train_normal': {'path': './data/train/normal/', 'number': 1341},
			'train_infected_covid': {'path': './data/train/infected/covid', 'number': 1345},
			'train_infected_non-covid': {'path': './data/train/infected/non-covid', 'number': 2530},
			'val_normal': {'path': './data/val/normal/', 'number': 8},
			'val_infected_covid': {'path': './data/val/infected/covid', 'number': 9},
			'val_infected_non-covid': {'path': './data/val/infected/non-covid', 'number': 8},
			'test_normal': {'path': './data/test/normal/', 'number': 234},
			'test_infected_covid': {'path': './data/test/infected/covid', 'number': 139},
			'test_infected_non-covid': {'path': './data/test/infected/non-covid', 'number': 242}
		}
		self.total = sum([datum['number'] for datum in self.dataset.values()])

		# set default transformation to be applied when dataloader calls this
		self.tx = transforms.Compose([transforms.ToTensor()])

	def describe(self):
		"""
		Descriptor function.
		Will print details about the dataset when called.
		"""
		newlinetab = "\n\t\t"
		description = f"""
	This is the Lung Dataset used for small project in 50.039 Theory and Practice of 
	Deep learning. It contains a total of {self.total}, each 
	size {self.img_size}. Images have been split into {len(self.groups)} groups:
	{', '.join([str(group) for group in self.groups])}.
	The images are stored in the following locations and each one contains the
	following number of images:
		{newlinetab.join(['- {}, in folder {}: {} images'.format(k, v['path'], v['number']) for k, v in self.dataset.items()])}
		"""
		print(description)

	def set_tx(self, tx):
		"""
		Setter function for transformations

		Parameters:
		- 
		"""
		self.tx = tx
		
	def open_img(self, cls, idx):
		"""
		Opens image with specified parameters.
		
		Parameters:
		- cls variable should be set to 'normal', 'infected_covid' or 'infected_non-covid'.
		- idx should be an integer with values between 0 and the maximal number of images in dataset, idx given will
			correspond to image in order.
		
		Returns loaded image as a normalized Numpy array.
		"""
		# Open file as before
		path_to_file = '{}/{}.jpg'.format(self.dataset['{}_{}'.format(self.name, cls)]['path'], idx)
		with open(path_to_file, 'rb') as f:
			# Convert to Numpy array and normalize pixel values by dividing by 255.
			im = np.asarray(Image.open(f))/255
		f.close()
		return im

	def show_img(self, cls, idx):
		"""
		Opens, then displays image with specified parameters.
		
		Parameters:
		- cls variable should be set to 'normal' or 'infected'.
		- idx should be an integer with values between 0 and the maximal number of images in dataset, idx given will
			correspond to image in order.
		"""
		
		# Open image
		im = self.open_img(cls, idx)
		
		# Display
		plt.imshow(im)

	def __len__(self):
		"""
		magic function that will be called by dataloader
		"""
		return self.total

	def __getitem__(self, idx):
		"""
		magic function that will be called by dataloader to return x and y for model usage 
		"""
		# get cls of object using data_indexes created in children classes
		y = [1 if idx < n else 0 for n in self.data_indexes].index(1)
		cls = self.classes[y]

		# init offset list to subtract off the given idx
		offset = [0] + self.data_indexes[:-1]

		# use cls and actual idx to get img_loc
		img_loc = '{}/{}.jpg'.format(self.dataset['{}_{}'.format(self.name, cls)]['path'], idx - offset[y])

		# open imge and do transformations
		image = Image.open(img_loc)
		x = self.tx(image) 

		return x, y

'''
Dataset object for the lung training dataset
'''
class lung_train_dataset(lung_dataset):
	def __init__(self):
		super().__init__()
		# setting all the attributes acc to the training data
		self.dataset = {
			'train_normal': {'path': './data/train/normal/', 'number': 1341},
			'train_infected_covid': {'path': './data/train/infected/covid', 'number': 1345},
			'train_infected_non-covid': {'path': './data/train/infected/non-covid', 'number': 2530}
		}
		self.total = sum([datum['number'] for datum in self.dataset.values()])
		self.max = max([datum['number'] for datum in self.dataset.values()])
		
		# data_indexes will be used in the __getitem__ function
		# for now it stores the final index of each class, eg: [1341, 1341+1345, 1341+1345+2530]
		data_numbers = [v['number'] for v in list(self.dataset.values())]
		self.data_indexes = [sum(data_numbers[:i]) for i, n in enumerate(data_numbers, 1)]

		# this will be used only in binary datasets 
		self.name = 'train'

	def describe(self):
		"""
		Descriptor function.
		Will print details about the dataset when called.
		"""
		newlinetab = "\n\t\t"
		description = f"""
	These are the data that will be used for training of the model. It contains a total of {self.total},
	each size {self.img_size}. 
	The images are stored in the following locations and each one contains the
	following number of images:
		{newlinetab.join(['- {}, in folder {}: {} images'.format(k, v['path'], v['number']) for k, v in self.dataset.items()])}
		"""
		print(description)

'''
Dataset object for the lung validation dataset
'''
class lung_val_dataset(lung_dataset):
	def __init__(self):
		super().__init__()
		# setting all the attributes acc to the training data
		self.dataset = {
			'val_normal': {'path': './data/val/normal/', 'number': 8},
			'val_infected_covid': {'path': './data/val/infected/covid', 'number': 9},
			'val_infected_non-covid': {'path': './data/val/infected/non-covid', 'number': 8}
		}
		self.total = sum([datum['number'] for datum in self.dataset.values()])
		self.max = max([datum['number'] for datum in self.dataset.values()])

		# data_indexes will be used in the __getitem__ function
		# for now it stores the final index of each class, eg: [8, 16, 24]
		data_numbers = [v['number'] for v in list(self.dataset.values())]
		self.data_indexes = [sum(data_numbers[:i]) for i, n in enumerate(data_numbers, 1)]


		# this will be used only in binary datasets 
		self.name = 'val'

	def describe(self):
		"""
		Descriptor function.
		Will print details about the dataset when called.
		"""
		newlinetab = "\n\t\t"
		description = f"""
	These are the data that will be used during validation. It contains a total of {self.total},
	each size {self.img_size}. 
	The images are stored in the following locations and each one contains the
	following number of images:
		{newlinetab.join(['- {}, in folder {}: {} images'.format(k, v['path'], v['number']) for k, v in self.dataset.items()])}
		"""
		print(description)

'''
Dataset object for the lung testing dataset
'''
class lung_test_dataset(lung_dataset):
	def __init__(self):
		super().__init__()
		# setting all the attributes acc to the training data
		self.dataset = {
			'test_normal': {'path': './data/test/normal/', 'number': 234},
			'test_infected_covid': {'path': './data/test/infected/covid', 'number': 139},
			'test_infected_non-covid': {'path': './data/test/infected/non-covid', 'number': 242}
		}
		self.total = sum([datum['number'] for datum in self.dataset.values()])
		self.max = max([datum['number'] for datum in self.dataset.values()])
		
		# data_indexes will be used in the __getitem__ function
		# for now it stores the final index of each class, eg: [234, 234+139, 234+139+242]
		data_numbers = [v['number'] for v in list(self.dataset.values())]
		self.data_indexes = [sum(data_numbers[:i]) for i, n in enumerate(data_numbers, 1)]

		# this will be used only in binary datasets 
		self.name = 'test'

	def describe(self):
		"""
		Descriptor function.
		Will print details about the dataset when called.
		"""
		newlinetab = "\n\t\t"
		description = f"""
	These are the data that will be used during testing. It contains a total of {self.total},
	each size {self.img_size}. 
	The images are stored in the following locations and each one contains the
	following number of images:
		{newlinetab.join(['- {}, in folder {}: {} images'.format(k, v['path'], v['number']) for k, v in self.dataset.items()])}
		"""
		print(description)

'''
Dataset object for the lung binary train dataset.
This dataset will provide the dataloader a positive and negative class, depending on specification 
'''
class lung_bin_train_dataset(lung_train_dataset):
	def __init__(self, positive_class, exclude_class=None):
		super().__init__()
		self.exclude_class = exclude_class
		self.positive_class = positive_class
		# if there are classes to be excluded, re-compute variables
		if exclude_class != None:
			self.exclude_class = exclude_class

			# remove from class
			classes = list(self.classes.items())
			del classes[exclude_class]
			self.classes = {k:v for (k,v) in classes}

			# remove from dataset
			dataset = list(self.dataset.items())
			del dataset[exclude_class]
			self.dataset = {k:v for (k,v) in dataset}
			self.total = sum([datum['number'] for datum in self.dataset.values()])
			self.max = max([datum['number'] for datum in self.dataset.values()])

			# calculate the cut-off idx for each class
			data_numbers = [v['number'] for v in list(self.dataset.values())]
			self.data_indexes = [sum(data_numbers[:i]) for i, n in enumerate(data_numbers, 1)]

		# positive class will be used in the __getitem__ function for the dataloader
		# only the positive class will be given a label of 1, other classes will be given a label of 0

	def __getitem__(self, idx):
		"""
		magic function that will be called by dataloader to return x and y for model usage 
		"""
		# get cls of object using data_indexes created in children classes
		y = [1 if idx < n else 0 for n in self.data_indexes].index(1)
		cls = list(self.classes.values())[y]

		# init offset list to subtract off the given idx
		offset = [0] + self.data_indexes[:-1]

		# use cls and actual idx to get img_loc
		img_loc = '{}/{}.jpg'.format(self.dataset['{}_{}'.format(self.name, cls)]['path'], idx - offset[y])

		# open imge and do transformations
		image = Image.open(img_loc)
		x = self.tx(image)

		if self.exclude_class == None:
			if y == self.positive_class:
				y = 1
			else:
				y = 0
		
		return x, y

'''
Dataset object for the lung binary valdation dataset.
This dataset will provide the dataloader a positive and negative class, depending on specification 
'''
class lung_bin_val_dataset(lung_val_dataset):
	def __init__(self, positive_class, exclude_class=None):
		super().__init__()
		self.exclude_class = exclude_class
		self.positive_class = positive_class
		# if there are classes to be excluded, re-compute variables
		if exclude_class != None:
			self.exclude_class = exclude_class

			# remove from class
			classes = list(self.classes.items())
			del classes[exclude_class]
			self.classes = {k:v for (k,v) in classes}

			# remove from dataset
			dataset = list(self.dataset.items())
			del dataset[exclude_class]
			self.dataset = {k:v for (k,v) in dataset}
			self.total = sum([datum['number'] for datum in self.dataset.values()])
			self.max = max([datum['number'] for datum in self.dataset.values()])

			# calculate the cut-off idx for each class
			data_numbers = [v['number'] for v in list(self.dataset.values())]
			self.data_indexes = [sum(data_numbers[:i]) for i, n in enumerate(data_numbers, 1)]

	def __getitem__(self, idx):
		"""
		magic function that will be called by dataloader to return x and y for model usage 
		"""
		# get cls of object using data_indexes created in children classes
		y = [1 if idx < n else 0 for n in self.data_indexes].index(1)
		cls = list(self.classes.values())[y]

		# init offset list to subtract off the given idx
		offset = [0] + self.data_indexes[:-1]

		# use cls and actual idx to get img_loc
		img_loc = '{}/{}.jpg'.format(self.dataset['{}_{}'.format(self.name, cls)]['path'], idx - offset[y])

		# open imge and do transformations
		image = Image.open(img_loc)
		x = self.tx(image) 

		if self.exclude_class == None:
			if y == self.positive_class:
				y = 1
			else:
				y = 0

		return x, y

'''
Dataset object for the lung binary test dataset.
This dataset will provide the dataloader a positive and negative class, depending on specification 
'''
class lung_bin_test_dataset(lung_test_dataset):
	def __init__(self, positive_class, exclude_class=None):
		super().__init__()
		# if there are classes to be excluded, re-compute variables
		self.exclude_class = exclude_class
		self.positive_class = positive_class
		if exclude_class != None:

			# remove from class
			classes = list(self.classes.items())
			del classes[exclude_class]
			self.classes = {k:v for (k,v) in classes}

			# remove from dataset
			dataset = list(self.dataset.items())
			del dataset[exclude_class]
			self.dataset = {k:v for (k,v) in dataset}
			self.total = sum([datum['number'] for datum in self.dataset.values()])
			self.max = max([datum['number'] for datum in self.dataset.values()])

			# calculate the cut-off idx for each class
			data_numbers = [v['number'] for v in list(self.dataset.values())]
			self.data_indexes = [sum(data_numbers[:i]) for i, n in enumerate(data_numbers, 1)]

	def __getitem__(self, idx):
		"""
		magic function that will be called by dataloader to return x and y for model usage 
		"""
		# get cls of object using data_indexes created in children classes
		y = [1 if idx < n else 0 for n in self.data_indexes].index(1)
		cls = list(self.classes.values())[y]

		# init offset list to subtract off the given idx
		offset = [0] + self.data_indexes[:-1]

		# use cls and actual idx to get img_loc
		img_loc = '{}/{}.jpg'.format(self.dataset['{}_{}'.format(self.name, cls)]['path'], idx - offset[y])

		# open imge and do transformations
		image = Image.open(img_loc)
		x = self.tx(image) 

		if self.exclude_class == None:
			if y == self.positive_class:
				y = 1
			else:
				y = 0

		return x, y


'''
utility function to load a full dataset into train, validation, and test dataloader

Parameters
- datasets <dict>: to contain 'train', 'test', and 'validation' keys which have the correspoding datasets
- params <dict>: to contain the parameters for the dataloader
'''
def load_data(datasets, params):
	trainloader = DataLoader(datasets['train'], **params)
	testloader = DataLoader(datasets['test'], **params)
	validloader = DataLoader(datasets['validation'], **params)

	return trainloader, validloader, testloader
