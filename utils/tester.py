'''
This script stores functions for testing data given a model.
'''

import torch
import numpy as np
from tqdm.auto import tqdm 

'''
test a multi-class model based on the test_loader, and loss function that is given

Parameters
- model <nn.Module>: the model to test on
- test_loader <Dataloader>: the dataloader object that contains the test dataset
- loss_fn <torch.nn.modules.loss>: loss function that defines the loss between the prediction and the truth
- device <str>: device to test on; 'cuda', 'cpu'
- thresh <float>: the threshold value for the data to be classified as a positive class

Returns:
- <float>: average test loss from current test
- <np.array>: predictions 
- <np.array>: truths
'''
def test_bin_model(model, test_loader, loss_fn, device, thresh=0.5):

    # move the model to the device and set it evaluation mode
    model.to(device)
    model.eval()

    predicted = []
    actual = []
    tl_loss = 0
    tl = 0
    print('Evaluating on given set')

    with torch.no_grad():
        for (X, y) in tqdm(test_loader):

            y = y.view(y.size(0), 1).float()

            # move the inputs and labels to device
            X, y = X.to(device), y.to(device)

            # get the output from the model
            output = model(X)

            # get the predictions
            ps = torch.sigmoid(model(X))
            preds = (ps > thresh).int().flatten()

            # add the predictions and actual to the lists
            predicted.extend(preds.cpu().detach())
            actual.extend(y.cpu().detach())

            # compute loss
            loss = loss_fn(output, y)
            tl_loss += loss.item()
            tl += X.size(0)

    print(f'Average test loss: {tl_loss / tl}')
    return tl_loss / tl, np.array(predicted), np.array(actual)
    
'''
stacks 2 models over each other and computes the outputs from both and compare them with the actual labels

Parameters
- model1 <nn.Module>: The first model that the data goes through
- model2 <nn.Module>: The second model that only the negative data from the first layer will go through
- model1_positive_class <int>: the class to be classified as positive in the dataset in the first layer
- model2_positive_class <int>: the class to be classified as positive in the dataset in the second layer
- test_loader <Dataloader>: the dataloader object that contains the test dataset
- loss_fn <torch.nn.modules.loss>: loss function that defines the loss between the prediction and the truth
- device <str>: device to test on; 'cuda', 'cpu'

Returns:
- <np.array>: predictions 
- <np.array>: truths
'''
def test_bin_class_model(model1, model2, model1_positive_class, model2_positive_class, test_loader, model1_thresh=0.5, model2_thresh=0.5,  device='cuda'):

    # set both models to be on the device and turn on evaluation mode
    model1.to(device)
    model2.to(device)
    model1.eval()
    model2.eval()
    
    predictions = []
    truth = []
    print('Evaluating on given set')

    with torch.no_grad():
        for (X, y) in tqdm(test_loader):
            X, y = X.to(device), y.to(device)
            y = y.view(y.size(0), 1).float()

            # get predictions for both models
            
            # get predictions for both models
            preds1 = (torch.sigmoid(model1(X)) > model1_thresh).int().flatten()
            preds2 = (torch.sigmoid(model2(X)) > model2_thresh).int().flatten()

            if model1_positive_class != 0:
                # multiply output from model1 so that that predictions correspond to the correct class
                preds1 = preds1 * model1_positive_class
                preds2 = preds2 * model2_positive_class
            else:
                # if model1_positve_class is 0, this means model2 is predicting class 1 and 2,
                # so we increase the predicted labels of them by 1, to get a list of 1's and 2's
                preds2 = preds2 + torch.ones(preds2.shape[0]).type(torch.int32).to(device)

            # init prediction tensor
            prediction = torch.zeros(preds1.shape, dtype=torch.int32).to(device)

            # loop through each value in preds1 and preds2 and replace prediction with the predicted label
            for pred1, pred2, pred in zip(preds1, preds2, prediction):
                if model1_positive_class != 0:
                    # As long as model1_positive_class is 1 or 2,
                    # add the values from preds1 to predictions
                    # then for all negative cases from preds1, change them to those from preds2
                    if pred1 == model1_positive_class:
                        pred += pred1
                    else:
                        pred += pred2
                else:
                    # if model1_positive_class is 0,
                    # replace all the 0's in preds1 with pred2 values
                    if pred1 == 0:
                        pred += pred2

            # predictions and truth for return
            predictions.extend(prediction.cpu().detach())
            truth.extend(y.cpu().detach())
            
    return np.array(predictions), np.array(truth)

'''
stacks 2 models over each other and computes the outputs from both and compare them with the actual labels

Parameters
- model <nn.Module>: the model that we want to get the probablilities from
- test_loader <Dataloader>: the dataloader object that contains the test dataset
- device <str>: device to test on; 'cuda', 'cpu'

Returns:
- <np.array>: predictions 
- <np.array>: truths
'''
def bin_get_probs(model, test_loader, device):
    probs = []
    actual = []

    # set model evaluation mode
    model.eval()
    print('Evaluating on test set')
    with torch.no_grad():
        for (X, y) in tqdm(test_loader):
            # convert labels to a comparable form
            y = y.view(y.size(0), 1).float()

            # put inputs and labels both to device
            X, y = X.to(device), y.to(device)

            # put the inputs through the model and put them through a sigmoid function
            ps = torch.sigmoid(model(X))

            # add the predictions and labels to their lists
            probs.extend(ps.cpu().detach().numpy())
            actual.extend(y.cpu().detach())
    return np.array(actual), np.array(probs)
